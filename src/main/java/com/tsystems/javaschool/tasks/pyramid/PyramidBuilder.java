package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be
     * build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        int D = discriminant(inputNumbers.size());
        if (!isValid(D)) throw new CannotBuildPyramidException();

        try {
            Collections.sort(inputNumbers);
        } catch (Exception e)
        {
            throw new CannotBuildPyramidException();
        }

        int stepNumber = (-1 + (int)Math.sqrt(D)) / 2;
        int baseWidth = stepNumber * 2 - 1;

        int [][] pyramid = new int[stepNumber][baseWidth];

        Iterator<Integer> iterator = inputNumbers.iterator();
        for(int i = 0, indent = baseWidth / 2; i < stepNumber; i++, indent--) {
            int counter = 0;

            for (int j = indent;; j += 2) {
                pyramid[i][j] = iterator.next();
                counter++;
                if (counter > i) break;
            }
        }
        return pyramid;
    }

    protected int discriminant(int c) {
        return 1 + 8 * c;
    }

    /*
    проанализировав зависимость количества элементов правильной пирамиды от числа ступеней,
    я вывела формулу size = 0.5 * x^2 + 0.5 * x
    таким образом, x^2 + x = 2 * size
    число элементов выстраивается в правильную пирамиду, когда квадратное уравнение решается в целых числах
     */

    private boolean isValid(double discriminant) {
        return (Math.sqrt(discriminant) - (int)Math.sqrt(discriminant) < 0.0000001);
    }
}
