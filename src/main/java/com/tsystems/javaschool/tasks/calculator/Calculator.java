package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {

        double result;
        try {
            List<Lexeme> lexemesList = lexAnalyze(statement);
            LexemeBuffer lexemes = new LexemeBuffer(lexemesList);
            result = expr(lexemes);
        }
        catch (RuntimeException re) {
            return null;
        }

        return setScale(result);
    }

    protected static String setScale(double result) {
        if (Double.POSITIVE_INFINITY == result) return null;
        if (result - (int)result < 0.0001) return String.valueOf((int)result);
        return String.valueOf(BigDecimal.valueOf(result).setScale(4, RoundingMode.HALF_UP).doubleValue());
    }

    protected double expr(LexemeBuffer lexemes) {
        Lexeme lexeme = lexemes.next();
        if (lexeme.type == LexemeType.EOF) throw new RuntimeException();
        else {
            lexemes.back();
            return plusMinus(lexemes);
        }
    }

    protected double plusMinus(LexemeBuffer lexemes) {
        double value = multDiv(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.type) {
                case OP_PLUS:
                    value += multDiv(lexemes);
                    break;
                case OP_MINUS:
                    value -= multDiv(lexemes);
                    break;
                case EOF:
                case RIGHT_BRACKET:
                    lexemes.back();
                    return value;
                default: throw new RuntimeException();
            }
        }
    }

    protected double multDiv(LexemeBuffer lexemes) {
        double value = number(lexemes);
        while (true) {
            Lexeme lexeme = lexemes.next();
            switch (lexeme.type) {
                case OP_MULT:
                    value *= number(lexemes);
                    break;
                case OP_DIV:
                    value /= number(lexemes);
                    break;
                case EOF:
                case RIGHT_BRACKET:
                case OP_PLUS:
                case OP_MINUS:
                    lexemes.back();
                    return value;
                default: throw new RuntimeException();
            }
        }
    }

    protected double number(LexemeBuffer lexemes) {
        Lexeme lexeme = lexemes.next();
        switch (lexeme.type) {
            case NUMBER:
                return Double.parseDouble(lexeme.value);
            case LEFT_BRACKET:
                double value = plusMinus(lexemes);
                lexeme = lexemes.next();
                if (lexeme.type != LexemeType.RIGHT_BRACKET) throw new RuntimeException();
                return value;
            default: throw new RuntimeException();
        }
    }

    protected enum LexemeType {
        LEFT_BRACKET, RIGHT_BRACKET,
        OP_PLUS, OP_MINUS, OP_MULT, OP_DIV,
        NUMBER,
        EOF
    }

    protected static class Lexeme {
        LexemeType type;
        String value;

        private Lexeme (LexemeType type, String value) {
            this.type = type;
            this.value = value;
        }

        private Lexeme (LexemeType type, Character value) {
            this.type = type;
            this.value = value.toString();
        }
    }

    protected List<Lexeme> lexAnalyze (String statement) {
        ArrayList<Lexeme> lexemes = new ArrayList<>();
        int pos = 0;
        while (pos < statement.length()) {
            char c = statement.charAt(pos);
            switch (c) {
                case '(':
                    lexemes.add(new Lexeme(LexemeType.LEFT_BRACKET, c));
                    pos++;
                    continue;
                case ')' :
                    lexemes.add(new Lexeme(LexemeType.RIGHT_BRACKET, c));
                    pos++;
                    continue;
                case '+' :
                    lexemes.add(new Lexeme(LexemeType.OP_PLUS, c));
                    pos++;
                    continue;
                case '-' :
                    lexemes.add(new Lexeme(LexemeType.OP_MINUS, c));
                    pos++;
                    continue;
                case '*' :
                    lexemes.add(new Lexeme(LexemeType.OP_MULT, c));
                    pos++;
                    continue;
                case '/' :
                    lexemes.add(new Lexeme(LexemeType.OP_DIV, c));
                    pos++;
                    continue;
                default:
                    if ((c >= '0' && c <= '9') || c == '.') {
                        StringBuilder sb = new StringBuilder();
                        do {
                            sb.append(c);
                            pos++;
                            if (pos >= statement.length()) break;
                            c = statement.charAt(pos);
                        } while ((c >= '0' && c <= '9') || c == '.');

                        lexemes.add(new Lexeme(LexemeType.NUMBER, sb.toString()));
                    } else
                    if (c != ' ') throw new RuntimeException();
                    else pos++;
            }
        }
        lexemes.add(new Lexeme(LexemeType.EOF, ""));
        return lexemes;
    }

    protected static class LexemeBuffer {
        int pos;
        List<Lexeme> lexemes;

        LexemeBuffer (List<Lexeme> lexemes) {
            this.lexemes = lexemes;
        }

        Lexeme next() {
            return lexemes.get(pos++);
        }

        void back() {
            pos--;
        }
    }
}