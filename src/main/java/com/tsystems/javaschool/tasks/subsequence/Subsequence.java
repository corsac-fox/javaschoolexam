package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null) throw new IllegalArgumentException();

        y.retainAll(x);
        y = Arrays.asList(y.stream().distinct().toArray());

        if (x.size() != y.size()) return false;

        Iterator xIterator = x.iterator();
        Iterator yIterator = y.iterator();

        while (xIterator.hasNext()) {
            if (!xIterator.next().equals(yIterator.next())) return false;
        }
        return true;
    }
}